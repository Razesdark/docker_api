defmodule DockerApi.MixProject do
  use Mix.Project

  def project do
    [
      app: :docker_api,
      version: "0.1.3",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      package: package(),
      name: "DockerApi",
      source_url: "https://gitlab.com/g_kenkun/docker_api",
      docs: [
        main: "readme",
        extras: ["README.md"]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
      {:tesla, "1.3.2"},
      {:jason, "~> 1.1"},
      {:deep_merge, "~> 1.0"},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
    ]
  end

  defp description() do
    "Docker Engine API wrapper."
  end

  defp package() do
    [
      maintainers: ["g_ken"],
      licenses: ["MIT"],
      links: %{ "Gitlab" => "https://gitlab.com/g_kenkun/docker_api"}
    ]
  end
end
