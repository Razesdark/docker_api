defmodule DockerApi.Connection do
  defstruct ipfamily: :local, unix_socket: '/var/run/docker.sock'

  use Tesla, only: [:get, :post, :put, :delete], docs: false

  plug Tesla.Middleware.JSON

  @moduledoc false

  def http_request(connection, method, path, opts \\ []) do
    connection
    |> Map.to_list()
    |> Keyword.delete(:__struct__)
    |> :httpc.set_options()

    url = "http://" <> path

    _http_request(method, url, opts)
  end

  defp _http_request(:get, url, opts) do
    get(url, opts)
  end

  defp _http_request(:post, url, opts) do
    {body, opts} = Keyword.split(opts, [:body])
    post(url, Keyword.get(body, :body) || [], opts)
  end

  defp _http_request(:put, url, opts) do
    {body, opts} = Keyword.split(opts, [:body])
    put(url, Keyword.get(body, :body), opts)
  end

  defp _http_request(:delete, url, opts) do
    delete(url, opts)
  end
end
