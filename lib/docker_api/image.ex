defmodule DockerApi.Image do
  defstruct id: nil, connection: %DockerApi.Connection{}

  alias DockerApi.Connection

  @moduledoc """

  """
  @moduledoc since: "0.1.2"

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ImageList

  ## Example

  ```elixir
  iex> DockerApi.Image.list()
  {:ok,
   [
     %DockerApi.Image{
       connection: %DockerApi.Connection{
         ipfamily: :local,
         unix_socket: '/var/run/docker.sock'
       },
       id: "72300a873c2ca11c70d0c8642177ce76ff69ae04d61a5813ef58d40ff66e3e7c"
     },
     %DockerApi.Image{
       connection: %DockerApi.Connection{
         ipfamily: :local,
         unix_socket: '/var/run/docker.sock'
       },
       id: "fce289e99eb9bca977dae136fbe2a82b6b7d4c372474c9235adc1741675f587e"
     }
   ]}

  ```
  """
  @doc since: "0.1.2"

  def list(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path = path_for(:json)
    with {:ok, response} <- Connection.http_request(connection, :get, path, opts),
         %Tesla.Env{status: 200, body: images} <- response
    do
      images = Enum.map(images, fn image ->  <<115, 104, 97, 50, 53, 54, 58, id :: binary>> = image["Id"]; %DockerApi.Image{id: id, connection: connection} end)
      {:ok, images}
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ImageBuild

  TODO: Writing example and change content type header.
  """
  @doc since: "0.1.2"

  def build(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path = "/build"
    with {:ok, response} <- Connection.http_request(connection, :post, path, opts),
         %Tesla.Env{status: 200} <- response
    do
      :ok
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/BuildPrune

  ## Example

  ```elixir
  iex> DockerApi.Image.delete_build_cache
  {:ok, %{"CachesDeleted" => nil, "SpaceReclaimed" => 0}}
  ```
  """
  @doc since: "0.1.2"

  def delete_build_cache(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path = "/build/prune"
    with {:ok, response} <- Connection.http_request(connection, :post, path, opts),
         %Tesla.Env{status: 200, body: body} <- response
    do
      {:ok, body}
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ImageCreate

  TODO: Writing example.
  """
  @doc since: "0.1.2"

  def create(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path = path_for(:create)
    with {:ok, response} <- Connection.http_request(connection, :post, path, opts),
         %Tesla.Env{status: 200} <- response
    do
      :ok
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ImageInspect

  TODO: Writing example.
  """
  @doc since: "0.1.2"

  def inspect(image = %DockerApi.Image{}, opts \\ []) do
    path = path_for(image, :json)
    with {:ok, response} <- Connection.http_request(image.connection, :get, path, opts),
         %Tesla.Env{status: 200, body: body} <- response
    do
      {:ok, body}
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ImageHistory

  TODO: Writing example.
  """
  @doc since: "0.1.2"

  def history(image = %DockerApi.Image{}, opts \\ []) do
    path = path_for(image, :history)
    with {:ok, response} <- Connection.http_request(image.connection, :get, path, opts),
         %Tesla.Env{status: 200, body: body} <- response
    do
      {:ok, body}
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ImagePush

  TODO: Writing example.
  """
  @doc since: "0.1.2"

  def push(image = %DockerApi.Image{}, opts \\ []) do
    path = path_for(image, :push)
    with {:ok, response} <- Connection.http_request(image.connection, :post, path, opts),
         %Tesla.Env{status: 200} <- response
    do
      :ok
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ImageTag

  TODO: Writing example.
  """
  @doc since: "0.1.2"

  def tag(image = %DockerApi.Image{}, opts \\ []) do
    path = path_for(image, :tag)
    with {:ok, response} <- Connection.http_request(image.connection, :post, path, opts),
         %Tesla.Env{status: 201} <- response
    do
      :ok
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ImageDelete

  TODO: Writing example.
  """
  @doc since: "0.1.2"

  def remove(image = %DockerApi.Image{}, opts \\ []) do
    path = path_for(image)
    with {:ok, response} <- Connection.http_request(image.connection, :delete, path, opts),
         %Tesla.Env{status: 200, body: body} <- response
    do
      {:ok, body}
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ImageSearch

  ## Example

  ```elixir
  iex> DockerApi.Image.search([query: [term: "elixir", limit: 1, filters: ["is-official": true]]])
  {:ok,
   [
     %{
       "description" => "Elixir is a dynamic, functional language for building scalable and maintainable applications.",
       "is_automated" => false,
       "is_official" => true,
       "name" => "elixir",
       "star_count" => 388
     }
   ]}
  ```
  """
  @doc since: "0.1.2"

  def search(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path = path_for(:search)
    with {:ok, response} <- Connection.http_request(connection, :get, path, opts),
         %Tesla.Env{status: 200, body: body} <- response
    do
      {:ok, body}
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ImagePrune

  ## Example

  ```elixir
  iex> DockerApi.Image.delete_unused_image()
  {:ok, %{"ImagesDeleted" => nil, "SpaceReclaimed" => 0}}
  ```
  """
  @doc since: "0.1.2"

  def delete_unused_image(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path = path_for(:prune)
    with {:ok, response} <- Connection.http_request(connection, :post, path, opts),
         %Tesla.Env{status: 200, body: body} <- response
    do
      {:ok, body}
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ImageCommit

  TODO: Writing example.
  """
  @doc since: "0.1.2"

  def commit(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path = "/commit"
    with {:ok, response} <- Connection.http_request(connection, :post, path, opts),
         %Tesla.Env{status: 201, body: body} <- response
    do
      {:ok, body}
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ImageGet

  TODO: Writing example.
  """
  @doc since: "0.1.2"

  def export(image = %DockerApi.Connection{}, opts \\ []) do
    path = path_for(image, :get)
    with {:ok, response} <- Connection.http_request(image.connection, :get, path, opts),
         %Tesla.Env{status: 200, body: body} <- response
    do
      {:ok, body}
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ImageGetAll

  TODO: Writing example.
  """
  @doc since: "0.1.2"

  def several(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path = path_for(:get)
    with {:ok, response} <- Connection.http_request(connection, :get, path, opts),
         %Tesla.Env{status: 200, body: body} <- response
    do
      {:ok, body}
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ImageLoad

  TODO: Writing example.
  """
  @doc since: "0.1.2"

  def import(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path = path_for(:load)
    with {:ok, response} <- Connection.http_request(connection, :post, path, opts),
         %Tesla.Env{status: 200, body: body} <- response
    do
      {:ok, body}
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  defp path_for(image = %DockerApi.Image{}, path) do
    "/images/#{image.id}/#{path}"
  end

  defp path_for(image = %DockerApi.Image{}) do
    "/images/#{image.id}"
  end

  defp path_for(path) do
    "/images/#{path}"
  end
end
