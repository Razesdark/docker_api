defmodule DockerApi.Container do
  defstruct id: nil, connection: %DockerApi.Connection{}

  alias DockerApi.Connection

  @moduledoc """
  This module support container api.

  ```elixir
  # List containers.
  iex> DockerApi.Container.list()
  {:ok,
   [
     %DockerApi.Container{
       connection: %DockerApi.Connection{
         ipfamily: :local,
         unix_socket: '/var/run/docker.sock'
       },
       id: "1a50869e463662b4546c4dda0606e25165482080969ec690826db809d3ae9a70"
     }
   ]}

  # Create a container.
  iex> {:ok, container} = DockerApi.Container.create([query: [name: "test_container"], body: %{Image: "ubuntu:latest", Tty: true}])
  {:ok,
   %DockerApi.Container{
     connection: %DockerApi.Connection{
       ipfamily: :local,
       unix_socket: '/var/run/docker.sock'
     },
     id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
   }}

  # Start a container.
  iex> DockerApi.Container.start(container)
  {:ok,
   %DockerApi.Container{
     connection: %DockerApi.Connection{
       ipfamily: :local,
       unix_socket: '/var/run/docker.sock'
     },
     id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
   }}

  # Stop a container.
  iex> DockerApi.Container.stop(container)
  {:ok,
   %DockerApi.Container{
     connection: %DockerApi.Connection{
       ipfamily: :local,
       unix_socket: '/var/run/docker.sock'
     },
     id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
   }}

  # Restart a container.
  iex> DockerApi.Container.restart(container)
  {:ok,
   %DockerApi.Container{
     connection: %DockerApi.Connection{
       ipfamily: :local,
       unix_socket: '/var/run/docker.sock'
     },
     id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
   }}

  # Inspect a container.
  iex> DockerApi.Container.inspect(container)
  {:ok,
   %{
     "AppArmorProfile" => "",
     "Args" => [],
     ...
   }}

  # Pause a container.
  iex> DockerApi.Container.pause(container)
  {:ok,
   %DockerApi.Container{
     connection: %DockerApi.Connection{
       ipfamily: :local,
       unix_socket: '/var/run/docker.sock'
     },
     id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
   }}

  # Unpause a container.
  iex> DockerApi.Container.unpause(container)
  {:ok,
   %DockerApi.Container{
     connection: %DockerApi.Connection{
       ipfamily: :local,
       unix_socket: '/var/run/docker.sock'
     },
     id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
   }}

  # List processes running inside a container.
  iex> DockerApi.Container.top(container)
  {:ok,
   %{
     "Processes" => [
       ["root", "2046", "2019", "1", "22:56", "pts/0", "00:00:00", "/bin/bash"]
     ],
     "Titles" => ["UID", "PID", "PPID", "C", "STIME", "TTY", "TIME", "CMD"]
   }}

  # Kill a container
  iex> DockerApi.Container.kill(container)
  {:ok,
   %DockerApi.Container{
     connection: %DockerApi.Connection{
       ipfamily: :local,
       unix_socket: '/var/run/docker.sock'
     },
     id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
   }}

  # Remove a container
  iex> DockerApi.Container.remove(container)
  :ok
  ```
  """
  @moduledoc since: "0.1.1"

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerList

  ## Example

  ```elixir
  iex> DockerApi.Container.list()
  {:ok,
   [
     %DockerApi.Container{
       connection: %DockerApi.Connection{
         ipfamily: :local,
         unix_socket: '/var/run/docker.sock'
       },
       id: "1a50869e463662b4546c4dda0606e25165482080969ec690826db809d3ae9a70"
     }
   ]}
  ```
  """
  @doc since: "0.1.1"

  def list(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path = path_for(:json)

    with {:ok, response} <- Connection.http_request(connection, :get, path, opts),
         %Tesla.Env{status: 200, body: body} <- response do
      containers =
        Enum.map(body, fn container ->
          %DockerApi.Container{id: container["Id"], connection: connection}
        end)

      {:ok, containers}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerCreate

  ## Example

  ```elixir
  iex> {:ok, container} = DockerApi.Container.create([query: [name: "test_container"], body: %{Image: "ubuntu:latest", Tty: true}])
  {:ok,
   %DockerApi.Container{
     connection: %DockerApi.Connection{
       ipfamily: :local,
       unix_socket: '/var/run/docker.sock'
     },
     id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
   }}
  ```
  """
  @doc since: "0.1.1"

  def create(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path = path_for(:create)

    with {:ok, response} <- Connection.http_request(connection, :post, path, opts),
         %Tesla.Env{status: 201, body: %{"Id" => id}} <- response do
      {:ok, %DockerApi.Container{id: id, connection: connection}}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: body} ->
        {:error, status, body}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerInspect

  ## Example

  ```elixir
  iex> DockerApi.Container.inspect(container)
  {:ok,
   %{
     "AppArmorProfile" => "",
     "Args" => [],
     ...
   }}
  ```
  """
  @doc since: "0.1.1"

  def inspect(container = %DockerApi.Container{}, opts \\ []) do
    path = path_for(container, :json)

    with {:ok, response} <- Connection.http_request(container.connection, :get, path, opts),
         %Tesla.Env{status: 200, body: body} <- response do
      {:ok, body}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerTop

  ## Example

  ```elixir
  iex> DockerApi.Container.top(container)
  {:ok,
   %{
     "Processes" => [
       ["root", "2046", "2019", "1", "22:56", "pts/0", "00:00:00", "/bin/bash"]
     ],
     "Titles" => ["UID", "PID", "PPID", "C", "STIME", "TTY", "TIME", "CMD"]
   }}
  ```
  """
  @doc since: "0.1.1"

  def top(container = %DockerApi.Container{}, opts \\ []) do
    path = path_for(container, :top)

    with {:ok, response} <- Connection.http_request(container.connection, :get, path, opts),
         %Tesla.Env{status: 200, body: body} <- response do
      {:ok, body}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerLogs

  Retrieves logs for a container.

  ## Example

  ```elixir
  iex> DockerApi.Container.logs(container, params: [ stream: "1", stdout: "1", stderr: "1"])
  {
    :ok,
    << >>
  }
  ```
  """

  def logs(container = %DockerApi.Container{}, opts) do
    path = path_for(container, :logs)

    with {:ok, response} <- Connection.http_request(container.connection, :get, path, opts),
         %Tesla.Env{status: 200, body: body} <- response do
      {:ok, body}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerLogs

  ## Example

  ```elixir
  iex> DockerApi.Container.changes(container)
  {:ok, nil}
  ```
  """
  @doc since: "0.1.1"

  def changes(container = %DockerApi.Container{}, opts \\ []) do
    path = path_for(container, :changes)

    with {:ok, response} <- Connection.http_request(container.connection, :get, path, opts),
         %Tesla.Env{status: 200, body: body} <- response do
      {:ok, body}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerExport
  """
  @doc since: "0.1.1"

  def export(container = %DockerApi.Container{}, opts) do
    path = path_for(container, :export)

    with {:ok, response} <- Connection.http_request(container.connection, :get, path, opts),
         %Tesla.Env{status: 200, body: body} <- response do
      {:ok, body}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  #  def stats(container = %DockerApi.Container{}, opts) do
  #
  #  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerResize
  """
  @doc since: "0.1.1"

  def resize(container = %DockerApi.Container{}, opts \\ []) do
    path = path_for(container, :resize)

    with {:ok, response} <- Connection.http_request(container.connection, :post, path, opts),
         %Tesla.Env{status: 200, body: body} <- response do
      {:ok, body}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerStart

  ## Example

  ```elixir
  iex> DockerApi.Container.start(container)
  {:ok,
   %DockerApi.Container{
    connection: %DockerApi.Connection{
      ipfamily: :local,
      unix_socket: '/var/run/docker.sock'
    },
    id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
   }}
  ```
  """
  @doc since: "0.1.1"

  def start(container = %DockerApi.Container{}, opts \\ []) do
    path = path_for(container, :start)

    with {:ok, response} <- Connection.http_request(container.connection, :post, path, opts),
         %Tesla.Env{status: 204} <- response do
      {:ok, container}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerStop

  ## Example

  ```elixir
  iex> DockerApi.Container.stop(container)
  {:ok,
   %DockerApi.Container{
    connection: %DockerApi.Connection{
      ipfamily: :local,
      unix_socket: '/var/run/docker.sock'
    },
    id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
   }}
  ```
  """
  @doc since: "0.1.1"

  def stop(container = %DockerApi.Container{}, opts \\ []) do
    path = path_for(container, :stop)

    with {:ok, response} <- Connection.http_request(container.connection, :post, path, opts),
         %Tesla.Env{status: 204} <- response do
      {:ok, container}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerRestart

  ## Example

  ```elixir
  iex> DockerApi.Container.restart(container)
  {:ok,
   %DockerApi.Container{
    connection: %DockerApi.Connection{
      ipfamily: :local,
      unix_socket: '/var/run/docker.sock'
    },
    id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
   }}
  ```
  """
  @doc since: "0.1.1"

  def restart(container = %DockerApi.Container{}, opts \\ []) do
    path = path_for(container, :restart)

    with {:ok, response} <- Connection.http_request(container.connection, :post, path, opts),
         %Tesla.Env{status: 204} <- response do
      {:ok, container}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerKill

  ## Example

  ```elixir
  iex> DockerApi.Container.kill(container)
  {:ok,
   %DockerApi.Container{
    connection: %DockerApi.Connection{
      ipfamily: :local,
      unix_socket: '/var/run/docker.sock'
    },
    id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
   }}
  ```
  """
  @doc since: "0.1.1"

  def kill(container = %DockerApi.Container{}, opts \\ []) do
    path = path_for(container, :kill)

    with {:ok, response} <- Connection.http_request(container.connection, :post, path, opts),
         %Tesla.Env{status: 204} <- response do
      {:ok, container}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerUpdate
  """
  @doc since: "0.1.1"

  def update(container = %DockerApi.Container{}, opts \\ []) do
    path = path_for(container, :update)

    with {:ok, response} <- Connection.http_request(container.connection, :post, path, opts),
         %Tesla.Env{status: 200, body: body} <- response do
      {:ok, body}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerRename
  """
  @doc since: "0.1.1"

  def rename(container = %DockerApi.Container{}, opts \\ []) do
    path = path_for(container, :rename)

    with {:ok, response} <- Connection.http_request(container.connection, :post, path, opts),
         %Tesla.Env{status: 204, body: body} <- response do
      {:ok, body}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerPause

  ## Example

  ```elixir
  iex> DockerApi.Container.pause(container)
  {:ok,
   %DockerApi.Container{
    connection: %DockerApi.Connection{
      ipfamily: :local,
      unix_socket: '/var/run/docker.sock'
    },
    id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
   }}
  ```
  """
  @doc since: "0.1.1"

  def pause(container = %DockerApi.Container{}, opts \\ []) do
    path = path_for(container, :pause)

    with {:ok, response} <- Connection.http_request(container.connection, :post, path, opts),
         %Tesla.Env{status: 204} <- response do
      {:ok, container}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerUnpause

  ## Example

  ```elixir
  iex> DockerApi.Container.pause(container)
  {:ok,
   %DockerApi.Container{
    connection: %DockerApi.Connection{
      ipfamily: :local,
      unix_socket: '/var/run/docker.sock'
    },
    id: "cb5972cf722babb4fd2099da051d5850c59c92f314dec8262209d580ce6d4d8c"
   }}
  ```
  """
  @doc since: "0.1.1"

  def unpause(container = %DockerApi.Container{}, opts \\ []) do
    path = path_for(container, :unpause)

    with {:ok, response} <- Connection.http_request(container.connection, :post, path, opts),
         %Tesla.Env{status: 204} <- response do
      {:ok, container}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  #  def attach(container = %DockerApi.Container{}, opts) do
  #
  #  end

  #  def attach_ws(container = %DockerApi.Container{}, opts) do
  #
  #  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerWait
  """
  @doc since: "0.1.1"

  def wait(container = %DockerApi.Container{}, opts \\ []) do
    path = path_for(container, :wait)

    with {:ok, response} <- Connection.http_request(container.connection, :post, path, opts),
         %Tesla.Env{status: 200, body: body} <- response do
      {:ok, body}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerDelete

  ## Example

  ```elixir
  iex> DockerApi.Container.pause(container)
  :ok
  ```
  """
  @doc since: "0.1.1"

  def remove(container = %DockerApi.Container{}, opts \\ []) do
    path = path_for(container)

    with {:ok, response} <- Connection.http_request(container.connection, :delete, path, opts),
         %Tesla.Env{status: 204} <- response do
      :ok
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  #  def head_archive(container = %DockerApi.Container{}, opts) do
  #
  #  end

  #  def get_archive(container = %DockerApi.Container{}, opts) do
  #
  #  end

  #  def put_archive(container = %DockerApi.Container{}, opts) do
  #
  #  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/ContainerPrune
  """
  @doc since: "0.1.1"

  def prune(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path = path_for(:prune)

    with {:ok, response} <- Connection.http_request(connection, :post, path, opts),
         %Tesla.Env{status: 200, body: body} <- response do
      {:ok, body}
    else
      {:error, any} ->
        {:error, any}

      %Tesla.Env{status: status, body: %{"message" => message}} ->
        {:error, %{status: status, message: message}}
    end
  end

  defp path_for(container, path) do
    "/containers/#{container.id}/#{path}"
  end

  defp path_for(container = %DockerApi.Container{}) do
    "/containers/#{container.id}"
  end

  defp path_for(path) do
    "/containers/#{path}"
  end
end
